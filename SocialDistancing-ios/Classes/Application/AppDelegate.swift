//
//  AppDelegate.swift
//  SocialDistancing-ios
//
//  Created by Mehtab Ahmed on 8/27/20.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        Utility.loginRootViewController()
        return true
    }
}

