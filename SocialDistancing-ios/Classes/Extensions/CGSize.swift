//
//  CGSize.swift
//  SocialDistancing-ios
//
//  Created by Mac on 01/10/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

extension CGPoint {
    var cgSize: CGSize { return CGSize(width: x, height: y) }
    
    func absolutePoint(in rect: CGRect) -> CGPoint {
        return CGPoint(x: x * rect.size.width, y: y * rect.size.height) + rect.origin
    }
}
