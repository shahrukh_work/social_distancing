//
//  UITextField.swift
//  ShaeFoodDairy
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

extension UITextField {
    var substituteFontName : String {
        get { return self.font!.fontName }
        set { self.font = UIFont(name: newValue, size: (self.font?.pointSize)!) }
    }
}
