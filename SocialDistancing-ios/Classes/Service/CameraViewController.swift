//
//  CameraViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 02/10/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import AVFoundation
import TensorFlowLite

class CameraViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    
    //MARK: - Variables
    private lazy var faceDetectionService = FaceDetectionService()
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        preProcessing ()
        faceDetectionService.delegate = self
        faceDetectionService.imageView = imageView
        faceDetectionService.prepare(previewView: previewView, cameraPosition: .back) { [weak self] _ in
            self?.faceDetectionService.start()
        }
    }
    
    
    //MARK: - Actions
    @IBAction func crossTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func preProcessing () {
       
        
        
        
//        let startTime = CFAbsoluteTimeGetCurrent()
//        let cgImage: CGImage = (self.image.cgImage)!// Your input image
//        var bytes = [UInt8](repeating: 0, count: 512 * 512 * 4)
//
//        guard let context = CGContext(
//            data: &bytes,
//            width: cgImage.width, height: cgImage.height,
//            bitsPerComponent: 8, bytesPerRow: cgImage.width * 4,
//            space: CGColorSpaceCreateDeviceRGB(),
//            bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
//            ) else { return }
//
//        context.draw(cgImage, in: CGRect(x: 0, y: 0, width: cgImage.width, height: cgImage.height))
//
//        guard var imageData = context.data else { return }
//
//        try! preprocessingInterpreter.resizeInput(at: 0, to: Tensor.Shape.init([512, 512, 4]) )
//        try! preprocessingInterpreter.allocateTensors()
//        try! preprocessingInterpreter.copy(Data(bytes), toInputAt: 0)
//        try! preprocessingInterpreter.invoke()
//
//        let output = try! preprocessingInterpreter.output(at: 0)
//        let outputImageData = UnsafeMutableBufferPointer<UInt8>.allocate(capacity: 512 * 512 * 3)
//        output.data.copyBytes(to: outputImageData)
//        efficientDetector(inputData: Data(outputImageData))
    }
}

extension CameraViewController: FaceDetectionDataDelegate {
    
    func receiveRespondingMessages(message: String, color: UIColor) {
        DispatchQueue.main.async {
//            self.errorMessageView.backgroundColor = color
            //self.errorMessageLabel.text = message
            //self.slider.tintColor = #colorLiteral(red: 0.9394901395, green: 0.2038211226, blue: 0.412638694, alpha: 1)
        }
    }
    
    func receiveImages(images: [UIImage]) {
//        self.faceImages = images
//
//        DispatchQueue.main.async {
//            self.slider.setValue(Float(self.faceImages.count), animated: true)
//            let percentage = String(format: "%.0f", (self.slider.value/10)*100)
//            self.showPercentageLabel.text = percentage + "%"
//
//            if percentage == "90" {
//                self.finishView.isHidden = false
//                self.showSliderView.isHidden = false
//            }
//            self.slider.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
//
//            if self.faceImages.count == 10 {
////                self.errorMessageView.backgroundColor = #colorLiteral(red: 0.4924473166, green: 0.4655112028, blue: 0.9140413404, alpha: 1)
//                self.errorMessageLabel.text = "Finishing up..."
//                //   self.faceDetectionService.stop()
//                _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startUploadingPhotos), userInfo: nil, repeats: false)
//            }
//        }
    }
}
