import UIKit
import Vision
import AVFoundation
import FaceCropper
import TensorFlowLite
import Accelerate

protocol CropDelegate: class {
    func getImage (rect: CGRect)
}

struct FaceElement {
    let points: [CGPoint]
    let needToClosePath: Bool
    func draw(in context: CGContext) {
        if points.isEmpty { return }
        context.addLines(between: points)
        if needToClosePath { context.closePath() }
        context.strokePath()
    }
}

class FaceView: UIView {
    
    var croppedImage: UIImage = UIImage()
    var cutImageRef: CGImage? 
    weak var delegate: CropDelegate?
    private var boundingBox: [CGRect] = []
    var image = UIImage()
    var imageView: UIImageView?
    var resultCount = 0
    var preprocessingInterpreter:Interpreter!
    var bytes = [UInt8](repeating: 0, count: 96 * 96 * 4)
    var cgImage : CGImage?
    var doOnce = false
    let preprocesserModelPath = Bundle.main.path(forResource: "maskclf", ofType: "tflite")
    func clearAndSetNeedsDisplay() {
        boundingBox = []
        DispatchQueue.main.async { [weak self] in self?.setNeedsDisplay() }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    
        for box in boundingBox {
          guard let context = UIGraphicsGetCurrentContext() else { return }

            context.saveGState()
            defer { context.restoreGState()}
            delegate!.getImage (rect: box)
            cropImage(self.image, toRect: box, viewWidth: self.frame.width , viewHeight: self.frame.height)
            context.clear(box)
            let color = processing() ? #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1) : #colorLiteral(red: 0.9394901395, green: 0.2038211226, blue: 0.412638694, alpha: 1)
            context.setStrokeColor(color.cgColor)
            context.setStrokeColor(color.cgColor)
            color.setStroke()
            context.stroke(box, width: 2)
            context.strokePath()
        }
    }
    
    func cropImage(_ inputImage: UIImage, toRect cropRect: CGRect, viewWidth: CGFloat, viewHeight: CGFloat)
    {
        
        let imageViewScale = max(inputImage.size.width / viewWidth,
                                 inputImage.size.height / viewHeight)
        
        // Scale cropRect to handle images larger than shown-on-screen size
        var width = cropRect.size.width * imageViewScale
        width = width + (width * 0.3)
        var height = cropRect.size.height * imageViewScale
        height = height + (height * 0.3)
        let x = (cropRect.origin.x - (width * 0.10)) * imageViewScale// - (width * 0.15)
        let y = (cropRect.origin.y - (height * 0.20)) * imageViewScale// - (height * 0.15)
        
        
        //                var width = cropRect.size.width * imageViewScale
        //                width = width + 30
        //                var height = cropRect.size.height * imageViewScale
        //                height = height + 30
        //                let x = cropRect.origin.x * imageViewScale - 15
        //                let y = cropRect.origin.y * imageViewScale - 50
        
        let cropZone = CGRect(x: x,
                              y: y,
                              width: width,
                              height: height)
        
        print("crop zone \(cropZone)")
        // Perform cropping in Core Graphics
        self.cutImageRef = self.resizeImage(image: inputImage, targetSize: inputImage.size).cgImage?.cropping(to:cropZone)
        // Return image to UIImage
        self.croppedImage = UIImage(cgImage: self.cutImageRef!)
        
    }

    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    func read(results: [VNFaceObservation], previewLayer: AVCaptureVideoPreviewLayer, image: UIImage, imageView: UIImageView) {
        defer { DispatchQueue.main.async { [weak self] in self?.setNeedsDisplay() } }
        resultCount = results.count
        boundingBox.removeAll()
        self.image = image
        self.imageView = imageView
                    
        DispatchQueue.main.async {
            let transform = CGAffineTransform(scaleX: 1, y: -1).translatedBy(x: 0, y: -self.frame.height)
            let translate = CGAffineTransform.identity.scaledBy(x: self.frame.width, y: self.frame.height)
            
            for result in results {
                let rect = result.boundingBox
                let facebounds = rect.applying(translate).applying(transform)
                self.boundingBox.append(facebounds)
            }
        }
    }
    
    func processing () -> Bool {
        
        imageView?.image = croppedImage
        
        guard let cgImage = croppedImage.cgImage else {
            return false
        }

        var bytes = [UInt8](repeating: 0, count: cgImage.width * cgImage.height * 4)
        
        guard let context = CGContext(
            data: &bytes,
            width: cgImage.width, height: cgImage.height,
            bitsPerComponent: 8, bytesPerRow: cgImage.width * 4,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
            ) else { return false}

        context.draw(cgImage, in: CGRect(x: 0, y: 0, width: cgImage.width, height: cgImage.height))
        
        if !doOnce {
            try! self.preprocessingInterpreter = Interpreter.init(modelPath: self.preprocesserModelPath!)
            doOnce = true
        }
        
        do {
            try preprocessingInterpreter.resizeInput(at: 0, to: Tensor.Shape.init([Int(cgImage.width), Int(cgImage.height), 4]) )
            try preprocessingInterpreter.allocateTensors()
            try preprocessingInterpreter.copy(Data(bytes), toInputAt: 0)
            try preprocessingInterpreter.invoke()
            
        } catch {
            print(error)
            return false
        }
        
        let output = try! preprocessingInterpreter.output(at: 0)
        let outputImageData = UnsafeMutableBufferPointer<Float32>.allocate(capacity: 1)
        _ = output.data.copyBytes(to: outputImageData)
        print(outputImageData[0])
        return (outputImageData[0]) > Float(0.0)
    }
}

extension CGSize {
    var cgPoint: CGPoint { return CGPoint(x: width, y: height) }
}

extension Data {
  /// Creates a new buffer by copying the buffer pointer of the given array.
  ///
  /// - Warning: The given array's element type `T` must be trivial in that it can be copied bit
  ///     for bit with no indirection or reference-counting operations; otherwise, reinterpreting
  ///     data from the resulting buffer has undefined behavior.
  /// - Parameter array: An array with elements of type `T`.
  init<T>(copyingBufferOf array: [T]) {
    self = array.withUnsafeBufferPointer(Data.init)
  }
}
