//
//  SelectViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 04/09/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import EMTNeumorphicView
import AVKit

class SelectViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var indoorView: EMTNeumorphicView!
    @IBOutlet weak var outdoorView: EMTNeumorphicView!
    @IBOutlet weak var previousView: EMTNeumorphicView!
    @IBOutlet weak var nextView: EMTNeumorphicView!
    @IBOutlet weak var engineTitleLabel: UILabel!
    @IBOutlet weak var neuCircleView: EMTNeumorphicView!
    @IBOutlet weak var circleAView: EMTNeumorphicView!
    @IBOutlet weak var circleBView: EMTNeumorphicView!
    @IBOutlet weak var circleCView: EMTNeumorphicView!
    @IBOutlet weak var menuButton: EMTNeumorphicView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var indoorPlayerView: UIView!
    @IBOutlet weak var outdoorPlayerView: UIView!
    

    
    //MARK: - Variable
    var engineType: EngineType = .coco
    var engineTypes: [EngineType] = [.coco, .maskDetection, .socialDistancing]
    
    
    var currentEngineIndex = 0
    private var player: AVQueuePlayer!
    private var playerLayer: AVPlayerLayer!
    private var playerItem: AVPlayerItem!
    private var playerLooper: AVPlayerLooper!
    var isFirstTime = true

    //For Indoor Small
    private var playerIndoor: AVQueuePlayer!
    private var playerLayerIndoor: AVPlayerLayer!
    private var playerItemIndoor: AVPlayerItem!
    private var playerLooperIndoor: AVPlayerLooper!
    
    
    //For Indoor Small
    private var playerOutdoor: AVQueuePlayer!
    private var playerLayerOutdoor: AVPlayerLayer!
    private var playerItemOutdoor: AVPlayerItem!
    private var playerLooperOutdoor: AVPlayerLooper!

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeUI()
        updateUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        createPlayer()
        createIndoorPlayer()
        createOutdoorPlayer()
    }
    
    //MARK: - Setup
    func setupView () {
        
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    
    //MARK: - Actions
    @IBAction func indoorPressed(_ sender: Any) {
        
        if engineType == .maskDetection {
            let controller = CameraViewController()
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        
        if engineType != .coco {
            self.showOkAlert("This feature is yet to be implemented")
            return
        }
        
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        vc?.engineType = engineType
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func outdoorPressed(_ sender: Any) {
        
        if engineType == .maskDetection {
            let controller = CameraViewController()
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        
        if engineType != .coco {
            self.showOkAlert("This feature is yet to be implemented")
            return
        }
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        vc?.engineType = engineType
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func previousPressed(_ sender: Any) {
        
        if currentEngineIndex > 0 {
            currentEngineIndex -= 1
        }
        engineType = engineTypes[currentEngineIndex]
        updateUI()
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        if currentEngineIndex < engineTypes.count - 1 {
            currentEngineIndex += 1
        }
        engineType = engineTypes[currentEngineIndex]
        updateUI()
    }
    
    
    //MARK: - Private Methods
    func customizeUI () {
        Utility.customizeView(indoorView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        Utility.customizeView(menuButton, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        Utility.customizeView(outdoorView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        Utility.customizeView(nextView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1), cornerRadius: 12)
        Utility.customizeView(previousView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1), cornerRadius: 12)
        Utility.customizeView(neuCircleView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1), true)
        
    }
    
    func updateUI() {
        
        UIView.transition(with: engineTitleLabel, duration: 0.4, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.engineTitleLabel.text = self?.engineType.rawValue
        }
        ,completion: nil)
        
        Utility.customizeView(circleAView, color: #colorLiteral(red: 0.1880513237, green: 0.1909433938, blue: 0.2477792752, alpha: 1), true)
        Utility.customizeView(circleBView, color: #colorLiteral(red: 0.1880513237, green: 0.1909433938, blue: 0.2477792752, alpha: 1), true)
        Utility.customizeView(circleCView, color: #colorLiteral(red: 0.1880513237, green: 0.1909433938, blue: 0.2477792752, alpha: 1), true)
        
        
        switch engineType {
        
        case .coco:
            Utility.customizeView(circleAView, color: #colorLiteral(red: 0.6392156863, green: 0.6901960784, blue: 0.768627451, alpha: 1), true)
            if !isFirstTime {
                replacePlayer(fileName: "full-tracking-outdoor")
                replaceIndoorPlayer(fileName: "full-tracking-Indoor")
                replaceOutdoorPlayer(fileName: "full-tracking-outdoor")
            } else {
                isFirstTime.toggle()
            }
            
        case .maskDetection:
            Utility.customizeView(circleBView, color: #colorLiteral(red: 0.6392156863, green: 0.6901960784, blue: 0.768627451, alpha: 1), true)
            replacePlayer(fileName: "mask-outdoor")
            replaceIndoorPlayer(fileName: "mask-indoor")
            replaceOutdoorPlayer(fileName: "mask-outdoor")
            
        case .socialDistancing:
            Utility.customizeView(circleCView, color: #colorLiteral(red: 0.6392156863, green: 0.6901960784, blue: 0.768627451, alpha: 1), true)
            replacePlayer(fileName: "social-distancing-outdoor")
            replaceIndoorPlayer(fileName: "social-distancing-indoor")
            replaceOutdoorPlayer(fileName: "social-distancing-outdoor")
        }
    }
    
    func createPlayer (fileName: String = "full-tracking-outdoor") {
        guard let path = Bundle.main.path(forResource: fileName, ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        player = AVQueuePlayer()
        playerLayer = AVPlayerLayer(player: player)
        playerItem = AVPlayerItem(url: pathURL)
        playerLooper = AVPlayerLooper(player: player, templateItem: playerItem,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = videoView.layer.bounds

        videoView.layer.insertSublayer(playerLayer, at: 1)
        player.play()
    }
    
    func createIndoorPlayer (fileName: String = "full-tracking-Indoor") {
        guard let path = Bundle.main.path(forResource: fileName, ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        self.playerIndoor = AVQueuePlayer()
        self.playerLayerIndoor = AVPlayerLayer(player: playerIndoor)
        self.playerItemIndoor = AVPlayerItem(url: pathURL)
        self.playerLooperIndoor = AVPlayerLooper(player: playerIndoor, templateItem: playerItemIndoor,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayerIndoor.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayerIndoor.frame = indoorPlayerView.layer.bounds

        indoorPlayerView.layer.insertSublayer(playerLayerIndoor, at: 1)
        playerIndoor.play()
    }
    
    
    func createOutdoorPlayer (fileName: String = "full-tracking-outdoor") {
        guard let path = Bundle.main.path(forResource: fileName, ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        playerOutdoor = AVQueuePlayer()
        playerLayerOutdoor = AVPlayerLayer(player: playerOutdoor)
        playerItemOutdoor = AVPlayerItem(url: pathURL)
        playerLooperOutdoor = AVPlayerLooper(player: playerOutdoor, templateItem: playerItemOutdoor,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayerOutdoor.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayerOutdoor.frame = outdoorPlayerView.layer.bounds

        outdoorPlayerView.layer.insertSublayer(playerLayerOutdoor, at: 1)
        playerOutdoor.play()
    }

    func replacePlayer (fileName: String) {
        guard let path = Bundle.main.path(forResource: fileName, ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        let player = AVQueuePlayer()
        let playerLayer = AVPlayerLayer(player: player)
        let playerItem = AVPlayerItem(url: pathURL)
        let playerLooper = AVPlayerLooper(player: player, templateItem: playerItem,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = videoView.layer.bounds
        videoView.layer.replaceSublayer(self.playerLayer, with: playerLayer)
        player.play()
        self.player = player
        self.playerLayer = playerLayer
        self.playerItem = playerItem
        self.playerLooper = playerLooper
    }
    
    func replaceIndoorPlayer (fileName: String) {
        guard let path = Bundle.main.path(forResource: fileName, ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        let player = AVQueuePlayer()
        let playerLayer = AVPlayerLayer(player: player)
        let playerItem = AVPlayerItem(url: pathURL)
        let playerLooper = AVPlayerLooper(player: player, templateItem: playerItem,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = indoorPlayerView.layer.bounds
        indoorPlayerView.layer.replaceSublayer(self.playerLayerIndoor, with: playerLayer)
        player.play()
        self.playerIndoor = player
        self.playerLayerIndoor = playerLayer
        self.playerItemIndoor = playerItem
        self.playerLooperIndoor = playerLooper
    }
    
    func replaceOutdoorPlayer (fileName: String) {
        guard let path = Bundle.main.path(forResource: fileName, ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        let player = AVQueuePlayer()
        let playerLayer = AVPlayerLayer(player: player)
        let playerItem = AVPlayerItem(url: pathURL)
        let playerLooper = AVPlayerLooper(player: player, templateItem: playerItem,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = outdoorPlayerView.layer.bounds
        outdoorPlayerView.layer.replaceSublayer(self.playerLayerOutdoor, with: playerLayer)
        player.play()
        self.playerOutdoor = player
        self.playerLayerOutdoor = playerLayer
        self.playerItemOutdoor = playerItem
        self.playerLooperOutdoor = playerLooper
    }
}
