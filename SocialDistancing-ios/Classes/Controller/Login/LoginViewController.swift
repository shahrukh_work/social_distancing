//
//  LoginViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 04/09/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import EMTNeumorphicView
import AVKit

class LoginViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var logoView: EMTNeumorphicView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var submitButton: EMTNeumorphicView!
    @IBOutlet weak var emailView: EMTNeumorphicView!
    @IBOutlet weak var passwordView: EMTNeumorphicView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    
    
    //MARK: - Variable
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.white, #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1)]//Colors you want to add
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        gradientLayer.frame = CGRect.zero
       return gradientLayer
    }()
    var isInSignupMode = false
    private var player: AVQueuePlayer!
    private var playerLayer: AVPlayerLayer!
    private var playerItem: AVPlayerItem!
    private var playerLooper: AVPlayerLooper!


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        createPlayer ()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = videoView.layer.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupView ()
        
    }
    
    
    //MARK: - Setup
    func setupView () {
        customizeUI ()
    }
    
    
    //MARK: - Actions
    @IBAction func submitPressed(_ sender: Any) {
        
        if isInSignupMode {
            self.navigationController?.pushViewController(GetCodeViewController(), animated: true)
            
        } else {
            self.navigationController?.pushViewController(SelectViewController(), animated: true)
        }
    }
    
    @IBAction func loginSignupPressed(_ sender: Any) {
        isInSignupMode.toggle()
        signupButton.setTitle(isInSignupMode ? "Login" : "Sign Up", for: .normal)
    }
    
    //MARK: - Methods
    func customizeUI () {
        Utility.customizeView(emailView, color: #colorLiteral(red: 0.2117647059, green: 0.2196078431, blue: 0.2823529412, alpha: 1))
        Utility.customizeView(submitButton, color: #colorLiteral(red: 0.1607843137, green: 0.1647058824, blue: 0.2196078431, alpha: 1))
        Utility.customizeView(passwordView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        Utility.customizeView(logoView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1), true)
        emailTextField.attributedPlaceholder = NSAttributedString(string: "email/phone",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "password",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        emailView.neumorphicLayer!.addSublayer(gradientLayer)
        
    }
    
    func createPlayer () {
        guard let path = Bundle.main.path(forResource: "Logo_main", ofType:"mov") else {
            debugPrint("Logo-Animation4.mp4 not found")
            return
        }
        
        let pathURL = URL(fileURLWithPath: path)
        let duration = Int64( ( (Float64(CMTimeGetSeconds(AVAsset(url: pathURL).duration)) *  10.0) - 1) / 10.0 )

        player = AVQueuePlayer()
        playerLayer = AVPlayerLayer(player: player)
        playerItem = AVPlayerItem(url: pathURL)
        playerLooper = AVPlayerLooper(player: player, templateItem: playerItem,
                                      timeRange: CMTimeRange(start: CMTime.zero, end: CMTimeMake(value: duration, timescale: 1)) )
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoView.layer.insertSublayer(playerLayer, at: 1)
        player.isMuted = true
        player.play()
    }
}
