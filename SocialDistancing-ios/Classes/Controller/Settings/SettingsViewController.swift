//
//  SettingsViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 11/09/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import EMTNeumorphicView

class SettingsViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var crossView: EMTNeumorphicView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    func setupView() {
        Utility.customizeView(crossView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
    }
    
    
    //MARK: - Actions
    @IBAction func crossPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
