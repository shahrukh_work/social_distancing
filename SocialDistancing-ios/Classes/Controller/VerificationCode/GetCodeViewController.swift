//
//  GetCodeViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 04/09/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import EMTNeumorphicView

class GetCodeViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var emailView: EMTNeumorphicView!
    @IBOutlet weak var verifyButtonView: EMTNeumorphicView!
    
    
    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeUI ()
    }
    
    //MARK: - Setup
    func setupView () {
        
    }
    
    
    //MARK: - Actions
    @IBAction func verifyPressed(_ sender: Any) {
        self.navigationController?.pushViewController(VerifyViewController(), animated: true)
    }
    
    
    //MARK: - Methods
    func customizeUI () {
        Utility.customizeView(emailView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        Utility.customizeView(verifyButtonView, color: #colorLiteral(red: 0.1607843137, green: 0.1647058824, blue: 0.2196078431, alpha: 1))
        emailTextfield.attributedPlaceholder = NSAttributedString(string: "email/phone",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        
    }
}
