//
//  ViewController.swift
//  MLmodelTest
//
//  Created by Mehtab Ahmed on 8/25/20.
//  Copyright © 2020 Codesorbit. All rights reserved.
//

import UIKit
import Firebase
import TensorFlowLite
import AVFoundation
import EMTNeumorphicView

class ViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var outPutImage: UIImageView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var crossView: EMTNeumorphicView!
    @IBOutlet weak var engineTypeLabel: UILabel!
    
    
    //MARK: - Variables
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var image: UIImage = UIImage()
    var resizeFactor:CGFloat = 0.0
    var totalWidth:CGFloat = 0.0
    var actualImage = UIImage()
    var engineType: EngineType = .coco
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .hd1280x720
        Utility.customizeView(crossView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        
        engineTypeLabel.text = engineType.rawValue
        
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
            
            DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
                self.captureSession.startRunning()
            }
            
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
            
            
            let dataOutputQueue = DispatchQueue(label: "ObjectDetection",
            qos: .userInitiated, attributes: [],
            autoreleaseFrequency: .workItem)
            
            
            let videoOutput = AVCaptureVideoDataOutput()
            videoOutput.setSampleBufferDelegate(self, queue: dataOutputQueue)
            videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]

            // Add the video output to the capture session
             if captureSession.canAddOutput(videoOutput) {
                 captureSession.addOutput(videoOutput)
             }

            

            
        }  catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    
    //MARK: - Actions
    @IBAction func crossPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func optionsPressed(_ sender: Any) {
        self.navigationController?.pushViewController(SettingsViewController(), animated: true)
    }
    
    //MARK: - Methods
    func preProcessing () {
        var preprocessingInterpreter:Interpreter!
        guard let preprocesserModelPath = Bundle.main.path(forResource: "efficientdet_preprocessor", ofType: "tflite") else {            print("model file not found")
            return
        }
        try! preprocessingInterpreter = Interpreter.init(modelPath: preprocesserModelPath)

        let startTime = CFAbsoluteTimeGetCurrent()
        let cgImage: CGImage = (self.image.cgImage)!// Your input image
        var bytes = [UInt8](repeating: 0, count: 512 * 512 * 4)

        guard let context = CGContext(
            data: &bytes,
            width: cgImage.width, height: cgImage.height,
            bitsPerComponent: 8, bytesPerRow: cgImage.width * 4,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
            ) else { return }

        context.draw(cgImage, in: CGRect(x: 0, y: 0, width: cgImage.width, height: cgImage.height))

        guard var imageData = context.data else { return }
        
        try! preprocessingInterpreter.resizeInput(at: 0, to: Tensor.Shape.init([512, 512, 4]) )
        try! preprocessingInterpreter.allocateTensors()
        try! preprocessingInterpreter.copy(Data(bytes), toInputAt: 0)
        try! preprocessingInterpreter.invoke()
        
        let output = try! preprocessingInterpreter.output(at: 0)
        let outputImageData = UnsafeMutableBufferPointer<UInt8>.allocate(capacity: 512 * 512 * 3)
        output.data.copyBytes(to: outputImageData)
        
        if let results = efficientDetector(inputData: Data(outputImageData)) {
            //maskDetector(inputData: Data(outputImageData), objectsList: results)
        }
    }
    
    
    func efficientDetector (inputData: Data) -> UnsafeMutableBufferPointer<Float32>? {
        
        var interpreter:Interpreter!
        
        guard let modelPath = Bundle.main.path(forResource: "efficientdet-d0", ofType: "tflite") else {
            print("model file not found")
            return nil
        }
        try! interpreter = Interpreter.init(modelPath: modelPath)
        
        try! interpreter.allocateTensors()
        try! interpreter.copy(inputData, toInputAt: 0)
        
        try! interpreter.invoke()
        
        let output = try! interpreter.output(at: 0)
        
        let probabilities =
            UnsafeMutableBufferPointer<Float32>.allocate(capacity: 700)
        output.data.copyBytes(to: probabilities)
        
        //let endTime = Date()

        //let difference = Calendar.current.dateComponents([.minute, .second, .nanosecond], from: startTime, to: endTime)

        //let formattedString = String(format: "%02ld%02ld", difference.minute!, difference.second!)

        //print("Time Difference:" + String(difference.nanosecond!))
        
        let filteredResults = filterDataByProbability(probabilities, threshold:0.4)
        //print("-----------FILTERED DATA--------\n\(filteredResults)\n--------------")
        
        DispatchQueue.main.async {
            self.drawImageWithRectangles(results: filteredResults, image: self.image)
        }
        
        return probabilities
    }
    
    func maskDetector (inputData: Data, objectsList:UnsafeMutableBufferPointer<Float32>) {
        
        var interpreter:Interpreter!
        
        guard let modelPath = Bundle.main.path(forResource: "facemask", ofType: "tflite") else {
            print("model file not found")
            return
        }
        
        
        try! interpreter = Interpreter.init(modelPath: modelPath)
        
        try! interpreter.resizeInput(at: 0, to: [0, Int(self.image.size.width), Int(self.image.size.height), 4])
        try! interpreter.allocateTensors()
        try! interpreter.copy(inputData, toInputAt: 0)
        try! interpreter.copy(Data(buffer: objectsList), toInputAt: 1)
        
        try! interpreter.invoke()
        
        let output = try! interpreter.output(at: 0)
        
        let probabilities =
            UnsafeMutableBufferPointer<Float32>.allocate(capacity: 700)
        output.data.copyBytes(to: probabilities)
        
        //let endTime = Date()

        //let difference = Calendar.current.dateComponents([.minute, .second, .nanosecond], from: startTime, to: endTime)

        //let formattedString = String(format: "%02ld%02ld", difference.minute!, difference.second!)

        //print("Time Difference:" + String(difference.nanosecond!))
        
        let filteredResults = filterDataByProbability(probabilities, threshold:0.5)
        //print("-----------FILTERED DATA--------\n\(filteredResults)\n--------------")
        
        DispatchQueue.main.async {
            self.drawImageWithRectangles(results: filteredResults, image: self.image)
        }
    }
    
    func setupLivePreview() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        videoPreviewLayer.connection?.automaticallyAdjustsVideoMirroring = false
        videoPreviewLayer.connection?.isVideoMirrored = false
        previewView.layer.addSublayer(videoPreviewLayer)
    }
    
    func drawImageWithRectangles(results:[[Float32]],image: UIImage)
    {
        let originalImage = actualImage
        outPutImage.image = actualImage
        
        let imageSize = originalImage.size
        let scale: CGFloat = 0
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
        let testImage = UIImage()
        testImage.draw(at: CGPoint.zero)
        
        if let context = UIGraphicsGetCurrentContext() {
            context.setStrokeColor(UIColor.green.cgColor)
            context.setLineWidth(5)
            
            for i in 0..<results.count
            {
                let a = resizeFactor
                let x = CGFloat(results[i][1]) * (imageSize.width / 512)
                let y = CGFloat(results[i][0]) * (imageSize.height / 512)
                let w = CGFloat(results[i][3])
                let h = CGFloat(results[i][2])
                let rectangle = CGRect(x: x, y: y, width: w, height: h)
                
                context.addRect(rectangle)
                context.stroke(rectangle)
                
                let label = NSAttributedString(string: cocoIdMap[Int(results[i][5])] ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 30, weight: .bold), NSAttributedString.Key.backgroundColor: UIColor.green])
                label.draw(at: CGPoint(x: x, y: y))
            }
            //UIColor.black.setFill()
            //UIRectFill(rectangle)
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            outPutImage.image = newImage
            
        }
        
    }
    
    func filterDataByProbability(_ probabilities: UnsafeMutableBufferPointer<Float32>, threshold: Float) -> [[Float32]] {
        
        var filteredData:[[Float32]] = []
        let array = convertTo2Darray(probabilities)
        for item in array {
            if item[4] >= threshold && item[5] != 75 && item[5] != 78 && item[5] != 70 {
                filteredData.append(item)
            }
        }
        return filteredData
    }
    
    func convertTo2Darray(_ probabilities: UnsafeMutableBufferPointer<Float32>) -> [[Float32]] {
        
        var arrayData:[[Float32]] = []
        var arr:[Float32] = []
        for i in (1..<700) {
            let mod = i % 7
            if i == 0 || mod != 0 {
                arr.append(probabilities[i])
            }
            else {
                arr.append(probabilities[i])
                arrayData.append(arr)
                arr = []
            }
        }
        print("------------------ARRAYS DATA------------------\n\(arrayData)\n------------------------------------------------")
        return arrayData
    }
    
    func getMaxProbabilityObject(_ data: [[Float32]]) -> [Float32] {
        var arrData = data
        let count = arrData.count
        for i in (0..<arrData.count){
            if arrData.count == 1 {
                break
            }
            var item = arrData[i]
            for j in (i+1..<count){
                    if item[4] < arrData[j][4] {
                        arrData.remove(at: arrData.firstIndex(of: item)!)
                        item = arrData[j]
                    }
                    else if item[4] > arrData[j][4] {
                        arrData.remove(at: j)
                    }
                    else {
                        arrData.remove(at: j)
                    }
            }
        }
        return arrData[0]
    }
}


//MARK: - AVCapturePhotoCaptureDelegate
extension ViewController: AVCapturePhotoCaptureDelegate {

    @IBAction func didTakePhoto(_ sender: Any) {
        
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)
        outPutImage.image = image
    }
}


//MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
     func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        
        if connection.isVideoMirroringSupported
        {
            connection.isVideoMirrored = true
        }
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
        let image : UIImage =  UIImage(ciImage: ciimage, scale: 1.0, orientation: .rightMirrored)
        let newImage = image
        let blacSpaceImage = newImage.scalePreservingAspectRatio()
        actualImage = newImage
        totalWidth = blacSpaceImage.size.width
        resizeFactor = (512/blacSpaceImage.size.width) / blacSpaceImage.scale
        let image512 = blacSpaceImage.resized(withPercentage: resizeFactor)
        self.image = image512!
        
        var startTime = Date()

        
        self.preProcessing()
        
        let endTime = Date()

        let difference = Calendar.current.dateComponents([.minute, .second, .nanosecond], from: startTime, to: endTime)
        
        print(difference)
    }
    
    func convert(cmage:CIImage) -> UIImage {
         let context:CIContext = CIContext.init(options: nil)
         let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
         let image:UIImage = UIImage.init(cgImage: cgImage)
         return image
    }
}

extension UIImage {
    func scalePreservingAspectRatio(targetSize: CGSize = CGSize(width: 720, height: 1280)) -> UIImage {
        // Determine the scale factor that preserves aspect ratio
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        let scaleFactor = min(widthRatio, heightRatio)
        
        // Compute the new image size that preserves aspect ratio
        let scaledImageSize = CGSize(
            width: size.height * scaleFactor,
            height: size.height * scaleFactor
        )

        // Draw and return the resized UIImage
        let renderer = UIGraphicsImageRenderer(
            size: scaledImageSize
        )

        let scaledImage = renderer.image { ctx in
            
            let rectanlge = CGRect(x: 0, y: 0, width: 280, height: 1280)
            let rectanlge2 = CGRect(x: 1000, y: 0, width: 280, height: 1280)
            ctx.cgContext.setFillColor(UIColor.black.cgColor)
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.setLineWidth(10)
            ctx.cgContext.addRect(rectanlge)
            self.draw(in: CGRect(x: 280, y: 0, width: 720, height: 1280))
            ctx.cgContext.addRect(rectanlge2)
            ctx.cgContext.drawPath(using: .fillStroke)
        }
        
        return scaledImage
    }
}


extension UIImage
{
    /// Given a required height, returns a (rasterised) copy
    /// of the image, aspect-fitted to that height.

    func aspectFittedToHeight(_ newHeight: CGFloat) -> UIImage
    {
        let scale = newHeight / self.size.height
        let newWidth = self.size.width * scale
        let newSize = CGSize(width: newWidth, height: newHeight)
        let renderer = UIGraphicsImageRenderer(size: newSize)

        return renderer.image { _ in
            self.draw(in: CGRect(origin: .zero, size: newSize))
        }
    }
    
}




extension UIImage {

    func resized(withPercentage percentage: CGFloat) -> UIImage? {

        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))

        }

    }
    
    /// Returns a image that fills in newSize
    func resizedImage(newSize: CGSize = CGSize(width: 512, height: 512)) -> UIImage? {
        guard size != newSize else { return self }

    let hasAlpha = false
    let scale: CGFloat = 0.0
    UIGraphicsBeginImageContextWithOptions(newSize, !hasAlpha, scale)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)

        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    /// Returns a resized image that fits in rectSize, keeping it's aspect ratio
    /// Note that the new image size is not rectSize, but within it.
//    func resizedImageWithinRect(rectSize: CGSize) -> UIImage? {
////        let widthFactor = size.width / rectSize.width
////        let heightFactor = size.height / rectSize.height
////
////        var resizeFactor = widthFactor
////        if size.height > size.width {
////            resizeFactor = heightFactor
////        }
////
////        let newSize = CGSize(width: size.width / resizeFactor, height: size.height / resizeFactor)
////        let resized = resizedImage(newSize: newSize, uiImage: { img in
////            return img
////        })
//
//
//    }
    
    public func resizeToRect(_ size : CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return resizedImage!
    }
}
