//
//  NeuView.swift
//  SocialDistancing-ios
//
//  Created by Mac on 04/09/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import Foundation

      //
//  NeuView.swift
//  NeuTest
//
//  Created by Umer Khan on 04/09/2020.
//  Copyright © 2020 Umer Khan. All rights reserved.
//
import UIKit
extension UIColor {
static let offWhite = UIColor.init(red: 225/255, green: 225/255, blue: 235/255, alpha: 1)
    //rgba(41, 42, 56, 1)
    static let offDark = #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1)
}

class NeuView: UIView {
    @IBOutlet weak var contentView: UIView!
    let darkShadow = CALayer()
    let lightShadow = CALayer()
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    func commonInit() {
        Bundle.main.loadNibNamed("NeuViewXIB", owner: self, options: nil)
        addSubview(contentView)
        setshadow()
    }
    
    func setshadow() {
        
        darkShadow.frame = self.bounds
        darkShadow.cornerRadius = self.bounds.width/2
        darkShadow.backgroundColor = UIColor.offDark.cgColor
        darkShadow.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        darkShadow.shadowOffset = CGSize(width: 10, height: 10)
        darkShadow.shadowOpacity = 1
        darkShadow.shadowRadius = 15
        self.layer.insertSublayer(darkShadow, at: 0)
        lightShadow.frame = self.bounds
        lightShadow.cornerRadius = self.bounds.width/2
        lightShadow.backgroundColor = UIColor.offDark.cgColor
        lightShadow.shadowColor = UIColor.white.withAlphaComponent(0.07).cgColor
        lightShadow.shadowOffset = CGSize(width: -3, height: -2)
        lightShadow.shadowOpacity = 1
        lightShadow.shadowRadius = 5
        self.layer.insertSublayer(lightShadow, at: 0)
    }
}
    
    
  
  

