//
//  FaceExtractViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 01/10/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import TensorFlowLite

class FaceExtractViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var croppedImage: UIImage = UIImage()
    var cutImageRef: CGImage?
    weak var delegate: CropDelegate?
    private var boundingBox: [CGRect] = []
    var image = UIImage()
    var resultCount = 0
    var preprocessingInterpreter:Interpreter!
    var bytes = [UInt8](repeating: 0, count: 96 * 96 * 4)
    var cgImage : CGImage?
    var doOnce = false
    let preprocesserModelPath = Bundle.main.path(forResource: "maskclf", ofType: "tflite")
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    private let captureSession = AVCaptureSession()
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    private let videoDataOutput = AVCaptureVideoDataOutput()
    private var drawings: [CAShapeLayer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCameraInput()
        self.showCameraFeed()
        self.getCameraFrames()
        self.captureSession.startRunning()
        
        let controller = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(controller)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer.frame = self.view.frame
    }
    
    func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection) {
        
        guard let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            debugPrint("unable to get image from sample buffer")
            return
        }
        self.detectFace(in: frame)
    }
    
    private func addCameraInput() {
        guard let device = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera, .builtInTrueDepthCamera],
            mediaType: .video,
            position: .back).devices.first else {
                fatalError("No back camera device found, please make sure to run SimpleLaneDetection in an iOS device and not a simulator")
        }
        let cameraInput = try! AVCaptureDeviceInput(device: device)
        self.captureSession.addInput(cameraInput)
    }
    
    private func showCameraFeed() {
        self.previewLayer.videoGravity = .resizeAspectFill
        self.view.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.view.frame
        self.view.bringSubviewToFront(imageView)
    }
    
    private func getCameraFrames() {
        self.videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]
        self.videoDataOutput.alwaysDiscardsLateVideoFrames = true
        self.videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "camera_frame_processing_queue"))
        self.captureSession.addOutput(self.videoDataOutput)
        guard let connection = self.videoDataOutput.connection(with: AVMediaType.video),
            connection.isVideoOrientationSupported else { return }
        connection.videoOrientation = .portrait
    }
    
    private func detectFace(in image: CVPixelBuffer) {
        let faceDetectionRequest = VNDetectFaceLandmarksRequest(completionHandler: { (request: VNRequest, error: Error?) in
            DispatchQueue.main.async {
                if let results = request.results as? [VNFaceObservation] {
                    self.handleFaceDetectionResults(results)
                } else {
                    self.clearDrawings()
                }
            }
        })
        
        self.image = UIImage(ciImage: CIImage(cvImageBuffer: image))
        //Old implementation left mirrored change it accordingly
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: image, orientation: .leftMirrored, options: [:])
        try? imageRequestHandler.perform([faceDetectionRequest])
    }
    
    private func handleFaceDetectionResults(_ observedFaces: [VNFaceObservation]) {
        
        self.clearDrawings()
        let facesBoundingBoxes: [CAShapeLayer] = observedFaces.flatMap({ (observedFace: VNFaceObservation) -> [CAShapeLayer] in
            let faceBoundingBoxOnScreen = self.previewLayer.layerRectConverted(fromMetadataOutputRect: observedFace.boundingBox)
            let faceBoundingBoxPath = CGPath(rect: faceBoundingBoxOnScreen, transform: nil)
            let faceBoundingBoxShape = CAShapeLayer()
            faceBoundingBoxShape.path = faceBoundingBoxPath
            cropImage(self.image, toRect: faceBoundingBoxOnScreen, viewWidth: self.view.frame.width, viewHeight: self.view.frame.height)
             imageView?.image = croppedImage
            let color = processing() ? #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1) : #colorLiteral(red: 0.9394901395, green: 0.2038211226, blue: 0.412638694, alpha: 1)
            faceBoundingBoxShape.fillColor = UIColor.clear.cgColor
            faceBoundingBoxShape.strokeColor = color.cgColor
            var newDrawings = [CAShapeLayer]()
            newDrawings.append(faceBoundingBoxShape)
            return newDrawings
        })
        
        facesBoundingBoxes.forEach({ faceBoundingBox in self.view.layer.addSublayer(faceBoundingBox) })
        self.drawings = facesBoundingBoxes
    }
    
    private func clearDrawings() {
        self.drawings.forEach({ drawing in drawing.removeFromSuperlayer() })
    }
    
    
    
    
    func processing () -> Bool {
        
       
        
        guard let cgImage = croppedImage.cgImage else {
            return false
        }

        var bytes = [UInt8](repeating: 0, count: cgImage.width * cgImage.height * 4)
        
        guard let context = CGContext(
            data: &bytes,
            width: cgImage.width, height: cgImage.height,
            bitsPerComponent: 8, bytesPerRow: cgImage.width * 4,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
            ) else {return false}

        context.draw(cgImage, in: CGRect(x: 0, y: 0, width: cgImage.width, height: cgImage.height))
        
        if !doOnce {
            try! self.preprocessingInterpreter = Interpreter.init(modelPath: self.preprocesserModelPath!)
            doOnce = true
        }
        
        do {
            try preprocessingInterpreter.resizeInput(at: 0, to: Tensor.Shape.init([Int(cgImage.width), Int(cgImage.height), 4]) )
            try preprocessingInterpreter.allocateTensors()
            try preprocessingInterpreter.copy(Data(bytes), toInputAt: 0)
            try preprocessingInterpreter.invoke()
            
        } catch {
            print(error)
            return false
        }
        
        let output = try! preprocessingInterpreter.output(at: 0)
        let outputImageData = UnsafeMutableBufferPointer<Float32>.allocate(capacity: 1)
        _ = output.data.copyBytes(to: outputImageData)
        print(outputImageData[0])
        return (outputImageData[0]) > Float(0.0)
    }
    
    
    func cropImage(_ inputImage: UIImage, toRect cropRect: CGRect, viewWidth: CGFloat, viewHeight: CGFloat)
    {
        
        let imageViewScale = max(inputImage.size.width / viewWidth,
                                 inputImage.size.height / viewHeight)
        
        // Scale cropRect to handle images larger than shown-on-screen size
        var width = cropRect.size.width * imageViewScale
        width = width + (width*0.35)
        var height = cropRect.size.height * imageViewScale
        height = height + (height*0.7)
        let x = (cropRect.origin.x * imageViewScale) - (width*0.15)
        let y = (cropRect.origin.y * imageViewScale) - (height*0.4)
        
        
        //                var width = cropRect.size.width * imageViewScale
        //                width = width + 30
        //                var height = cropRect.size.height * imageViewScale
        //                height = height + 30
        //                let x = cropRect.origin.x * imageViewScale - 15
        //                let y = cropRect.origin.y * imageViewScale - 50
        
        let cropZone = CGRect(x: x,
                              y: y,
                              width: width,
                              height: height)
        
        print("crop zone \(cropZone)")
        // Perform cropping in Core Graphics
        self.cutImageRef = self.resizeImage(image: inputImage, targetSize: inputImage.size).cgImage?.cropping(to:cropZone)
        // Return image to UIImage
        self.croppedImage = UIImage(cgImage: self.cutImageRef!)
        print("image is cropped")
        
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    @objc func imageTapped() {
        UIImageWriteToSavedPhotosAlbum(imageView.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            print(error)
        } else {
            print("saved")
        }
    }
}
