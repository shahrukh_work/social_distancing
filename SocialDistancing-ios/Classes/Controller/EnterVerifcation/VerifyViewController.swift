//
//  VerifyViewController.swift
//  SocialDistancing-ios
//
//  Created by Mac on 04/09/2020.
//  Copyright © 2020 revolveAI. All rights reserved.
//

import UIKit
import EMTNeumorphicView

class VerifyViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var codeView: EMTNeumorphicView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var resendButton: UIButton!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeUI()
    }
    
    
    //MARK: - Setup
    func setupView () {
        
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    @IBAction func verifyCode(_ sender: Any) {
        if (codeTextField.text ?? "").count == 5 {
            self.navigationController?.pushViewController(SelectViewController(), animated: true)
        }
    }
    
    
    //MARK: - Methods
    func customizeUI () {
        let attrb = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.7647058824, green: 0.8, blue: 0.8470588235, alpha: 1),
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        let attributeString = NSMutableAttributedString(string: "Resend",
                                                        attributes: attrb)
        resendButton.setAttributedTitle(attributeString, for: .normal)

        
        Utility.customizeView(codeView, color: #colorLiteral(red: 0.2128536999, green: 0.2201716006, blue: 0.2836359441, alpha: 1))
        codeTextField.attributedPlaceholder = NSAttributedString(string: "Enter verification code",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        
    }
}
